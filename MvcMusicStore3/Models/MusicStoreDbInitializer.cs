﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime.MediaTypeNames;
using System.Web;
using System.Web.UI.WebControls;

namespace MvcMusicStore3.Models
{
    public class MusicStoreDbInitializer : System.Data.Entity.DropCreateDatabaseAlways<MusicStoreDB>
    {
        protected override void Seed(MusicStoreDB context)
        {
            context.Artists.Add(new Artist {Name = "Ed Sheran"});
            context.Genres.Add(new Genre {Name = "acoustic pop"});
            context.Albums.Add(new Album
            {
                Artist = new Artist { Name = "Fun"},
                Genre = new Genre {Name = "pop"},
                Price = 9.99m,
                Title = "Funny"

            });
        }
    }
}